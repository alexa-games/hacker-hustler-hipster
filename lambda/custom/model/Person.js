// export default class Person {
class Person {
    /**
     * 
     * @param {string} name 
     * @param {Number} costPerCycle 
     * @param {Number} equity 
     * @param {Number} proficiency 
     */
    constructor(name, costPerCycle, equity, proficiency){
        this.name = name;
        this.costPerCycle = costPerCycle;
        this.equity = equity;
        this.proficiency = proficiency;
    }

    /**
     * 
     * @returns {string}
     */
    toVoice(){
        const equity = this.equity > 0 ? `requires an equity of ${this.equity}` : 'requires no equity';
        const cost = this.costPerCycle > 0 ? `has a cost of ${this.costPerCycle} per cycle` : 'has no cost per cycle';
        const rank = `has a prodiciency on their field of ${this.proficiency} out of 100`;
        return `${this.name} ${rank}, ${cost} and ${equity}`;
    }
}
module.exports = Person;
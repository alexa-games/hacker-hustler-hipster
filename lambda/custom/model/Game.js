const State = require("./State");
const Stage = require("./Stage");
const Person = require("./Person");

const StageOptions = {
    hacker: 'hacker',
    hustler: 'hustler',
    hipster: 'hipster'
};

class Game {
    /**
     * 
     * @param {State} [state] 
     */
    constructor(state) {
        this.initialState = state instanceof State ? state : null;
        this.state = this.initialState ? this.initialState : null;
        this.stages = [];
        this.stage = null;
        this.hacker = null;
        this.hustler = null;
        this.hipster = null;
    }

    /**
     * 
     * @param {Object} json 
     * @returns {Game}
     */
    static regenerateGame(json) {
        if (typeof json === 'string') {
            json = JSON.parse(json);
        }
        let game = new Game();
        game.initialState = new State(json.initialState.name, json.initialState.money, json.initialState.popularity, json.initialState.mvp);
        game.state = new State(json.state.name, json.state.money, json.state.popularity, json.state.mvp)
        json.stages.forEach(x => {
            const result = x.result ? new State(x.result.name, x.result.money, x.result.popularity, x.result.mvp) : null;
            const change = x.change ? new State(x.change.name, x.change.money, x.change.popularity, x.change.mvp) : null;
            game.stages.push(new Stage(x.name, result, change));
        });
        game.stage = json.stage;
        game.hacker = json.hacker ? new Person(json.hacker.name, json.hacker.costPerCycle, json.hacker.equity, json.hacker.proficiency) : null;
        game.hustler = json.hustler ? new Person(json.hustler.name, json.hustler.costPerCycle, json.hustler.equity, json.hustler.proficiency) : null;
        game.hipster = json.hipster ? new Person(json.hipster.name, json.hipster.costPerCycle, json.hipster.equity, json.hipster.proficiency) : null;
        return game;
    }

    /**
     * @returns {State[]}
     */
    static getProjectOptions() {
        return [
            new State('Car sharing', 10000, 5, 0),
            new State('Movie streaming', 20000, 3, 0),
            new State('Social network', 5000, 10, 0)
        ];
    }

    /**
     * @returns {Stage[]}
     */
    getStageOptions() {
        return Object.values(StageOptions).filter(x => !this.stages.includes(y => y.name === x)).map(x => new Stage(x));
    }

    getPeopleOptions() {
        if(!this.stage){
            return null;
        }
        return [new Person('person 1', 1000, 0, 50), new Person('person 2', 2000, 0, 70), new Person('person 3', 300, 20, 65)];
    }

    endGame() {

    }

    /**
     * 
     * @param {Stage} stage 
     */
    setStage(stage) {
        this.stages.push(stage);
        this.stage = StageOptions[stage.name];
    }

    /**
     * 
     * @param {Person} person 
     */
    setPerson(person) {
        if(!this.stage){
            throw new Error('No stage set');
        }
        this[this.stage] = person;
    }

    getStatus() {

    }

    processStage() {
        let stage = this.stages.find(x => x.name === this.stage);
        //stage.change = new State('', this[this.stage])
        this.stage = undefined;
    }

    /**
     * 
     * @returns {string}
     */
    toString() {
        return JSON.stringify(this);
    }
}

module.exports = Game;
const State = require("./State");

// export default class Stage {
class Stage {
    /**
     * 
     * @param {string} name 
     * @param {State} [result] 
     * @param {State} [change] 
     */
    constructor(name, result, change){
        this.name = name;
        this.result = result;
        this.change = change;
    }
}

module.exports = Stage;
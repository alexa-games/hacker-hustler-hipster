class State {
    /**
     * 
     * @param {string} name 
     * @param {Number} money 
     * @param {Number} popularity 
     * @param {Number} mvp 
     */
    constructor(name, money, popularity, mvp) {
        this.name = name;
        this.money = money;
        this.popularity = popularity;
        this.mvp = mvp;
    }

    /**
     * 
     * @returns {string}
     */
    toVoice() {
        const money = this.money > 0 ? `has funding of ${this.money} dollars` : 'has no funding';
        const popularity = `has a popularity rating of ${this.proficiency} out of 100`;
        const mvp = `has an MVP rating of ${this.proficiency} out of 100`;
        return `${this.name} ${money}, ${popularity} and ${mvp}`;
    }
}

module.exports = State;
const GameLogic = require("../GameLogic.js");

class GameHandlers { }

GameHandlers.GetOptions = {
    canHandle: (handlerInput) => {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'GetOptions'
    },
    handle: async (handlerInput) => {
        const gameLogic = new GameLogic(handlerInput);
        let projectOptions = gameLogic.getOptions();
        let speechOutput;
        projectOptions.array.forEach(element => {
             speechOutput += element.toVoice();
        });
        const reprompt = 'Select an option bitch';
        return handlerInput.responseBuilder.speak(speechOutput).reprompt(reprompt).getResponse();
    },
};

module.exports = GameHandlers;
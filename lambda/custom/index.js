const GameLogic = require("./GameLogic");
const CommonHandlers = require("./handlers/CommonHandlers");
const GameHandlers = require("./handlers/GameHandlers");
const Alexa = require('ask-sdk');
const SKILL_NAME = 'Hacker Hustler Hipster';
const ddbTableName = 'hhh-games';
const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
  .withPersistenceAdapter(GameLogic.getPersistenceAdapter(ddbTableName))
  .addRequestHandlers(
    CommonHandlers.LaunchRequest,
    GameHandlers.GetOptions
  )
  .addRequestInterceptors(GameLogic.getUserDataInterceptor)
  .addErrorHandlers(CommonHandlers.ErrorHandler)
  .lambda();
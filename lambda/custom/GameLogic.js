const ddbAdapter = require('ask-sdk-dynamodb-persistence-adapter');
const Game = require("./model/Game");

class GameLogic {

    constructor(handlerInput) {
        if (!handlerInput)
            throw "A handlerInput is required for GameLogic to work.";
        this.handlerInput = handlerInput;
    }

    static  getPersistenceAdapter(tableName) {
        return new ddbAdapter.DynamoDbPersistenceAdapter({
            tableName: tableName,
            createTable: true,
        });
    }

    getSessionAttributes() {
        return this.handlerInput.attributesManager.getSessionAttributes();
    }

    setSessionAttributes(attributes) {
        this.handlerInput.attributesManager.setSessionAttributes(attributes);
    }

    setPersistentAttributes(attributes) {
        this.handlerInput.attributesManager.setPersistentAttributes(attributes);
    }

    async savePersistentAttributes() {
        this.handlerInput.attributesManager.savePersistentAttributes();
    }

    isFirstRun() {
        let attributes = this.getSessionAttributes();
        return attributes[GameLogic.attributes.firstRun] ? true : false;
    }

    /**
     * @returns {Game}
     */
    getCurrentGame() {
        let attributes = this.getSessionAttributes();
        return attributes[GameLogic.attributes.currentGame];
    }

    getOptions(){
        /**
         * Loucho implementara aquí
         */
    }

    setSelection(selection){
        /**
         * Loucho implementara acá también
         */
    }

    async saveGame(game, mode) {
        if (!mode)
            throw "The mode is needed to save the game";
        let attributes = this.getSessionAttributes();
        attributes[GameLogic.attributes.currentGame] = game;
        switch (mode) {
            case GameLogic.saveMode.session:
                this.setSessionAttributes(attributes);
                break;
            case GameLogic.saveMode.persistent:
            default:
                console.info('Saving to Dynamo: ', attributes);
                if (attributes[GameLogic.attributes.firstRun]) {
                    attributes[GameLogic.attributes.firstRun] = false;
                }
                this.setSessionAttributes(attributes);
                attributes[GameLogic.attributes.currentGameJSON] = attributes[GameLogic.attributes.currentGame];
                delete attributes[GameLogic.attributes.currentGame];
                delete attributes[GameLogic.attributes.isInitialized];
                this.setPersistentAttributes(attributes);
                await this.savePersistentAttributes();
                break;
        }
    }
}

GameLogic.saveMode = {
    session: 'session',
    persistent: 'persistent'
}

GameLogic.attributes = {
    firstRun: 'firstRun',
    isInitialized: 'isInitialized',
    currentGame: 'currentGame',
    currentGameJSON: 'currentGameJSON'
}

GameLogic.getUserDataInterceptor = {
    process: async (handlerInput) => {
        if (!handlerInput)
            throw "A handlerInput is required for getUserDataInterceptor to work.";
        let attributes = handlerInput.attributesManager.getSessionAttributes();
        if (handlerInput.requestEnvelope.request.type === 'LaunchRequest' || !attributes[GameLogic.attributes.isInitialized]) {
            persistentAttributes = await handlerInput.attributesManager.getPersistentAttributes();
            console.info('Getting from Dynamo : ', persistentAttributes);
            let sessionAttributes = persistentAttributes;
            if (persistentAttributes[GameLogic.attributes.firstRun] === undefined) {
                sessionAttributes[GameLogic.attributes.firstRun] = true;
            }
            sessionAttributes[GameLogic.attributes.currentGame] = persistentAttributes[GameLogic.attributes.currentGameJSON] ? Game.regenerateGame(persistentAttributes[GameLogic.attributes.currentGameJSON]) : undefined;
            sessionAttributes[GameLogic.attributes.isInitialized] = true;
            console.info('Esto es lo que traigo para session', sessionAttributes);
            handlerInput.attributesManager.setSessionAttributes(sessionAttributes);
        }
    }
}

module.exports = GameLogic;